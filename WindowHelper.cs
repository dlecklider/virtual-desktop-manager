using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;
using System.Text;
using WindowsDesktop;

namespace VirtualDesktopManager
{
    using HWND = IntPtr;

    /// <summary>Contains functionality to get all the open windows.</summary>
    public static class WindowHelper
    {
        /// <summary>Returns a dictionary that contains the handle and title of all the open windows.</summary>
        /// <returns>A dictionary that contains the handle and title of all the open windows.</returns>
        public static IDictionary<HWND, string> GetOpenWindows()
        {
            HWND shellWindow = GetShellWindow();
            Dictionary<HWND, string> windows = new Dictionary<HWND, string>();

            EnumWindows(delegate (HWND hWnd, int lParam)
            {
                if (hWnd == shellWindow) return true;
                if (!IsWindowVisible(hWnd)) return true;

                int length = GetWindowTextLength(hWnd);
                if (length == 0) return true;

                StringBuilder builder = new StringBuilder(length);
                GetWindowText(hWnd, builder, length + 1);

                windows[hWnd] = builder.ToString();
                return true;

            }, 0);

            return windows;
        }

        public static int GetDesktopIndex(VirtualDesktop desktop)
        {
            int index = -1;
            var desktops = VirtualDesktop.GetDesktops();
            var desktopCount = desktops.Length;

            for (int i = 0; i < desktopCount; i++)
            {
                var indexedDesktop = desktops[i];

                if (desktop.Equals(indexedDesktop))
                {
                    index = i + 1; // Virtual desktops are 1 indexed as apparent to the user
                }
            }

            return index;
        }

        public static void SwitchToDesktopIfExists(int desktopNumber)
        {
            var desktops = VirtualDesktop.GetDesktops();

            if (desktops.Length > desktopNumber)
            {
                desktops[desktopNumber].Switch();
            }
        }

        public static void MoveToDesktopIfExists(int desktopNumber)
        {
            var desktops = VirtualDesktop.GetDesktops();
            var foregroundWindow = GetForegroundWindow();
            var windows = GetOpenWindows();

            if (desktops.Length > desktopNumber && windows.ContainsKey(foregroundWindow))
            {
                var desktop = desktops[desktopNumber];
                VirtualDesktop.MoveToDesktop(foregroundWindow, desktop);
                desktop.Switch();
            }
        }

        public static void MoveLeftIfExists()
        {
            var currentDesktop = VirtualDesktop.Current;
            var leftDesktop = currentDesktop.GetLeft();

            if (leftDesktop != null)
            {
                var foregroundWindow = GetForegroundWindow();
                VirtualDesktop.MoveToDesktop(foregroundWindow, leftDesktop);
                leftDesktop.Switch();
            }
        }

        public static void MoveRightIfExists()
        {
            var currentDesktop = VirtualDesktop.Current;
            var rightDesktop = currentDesktop.GetRight();

            if (rightDesktop != null)
            {
                var foregroundWindow = GetForegroundWindow();
                VirtualDesktop.MoveToDesktop(foregroundWindow, rightDesktop);
                rightDesktop.Switch();
            }
        }

        private delegate bool EnumWindowsProc(HWND hWnd, int lParam);

        [DllImport("USER32.DLL")]
        private static extern IntPtr GetForegroundWindow();

        [DllImport("USER32.DLL")]
        private static extern bool EnumWindows(EnumWindowsProc enumFunc, int lParam);

        [DllImport("USER32.DLL")]
        private static extern int GetWindowText(HWND hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("USER32.DLL")]
        private static extern int GetWindowTextLength(HWND hWnd);

        [DllImport("USER32.DLL")]
        private static extern bool IsWindowVisible(HWND hWnd);

        [DllImport("USER32.DLL")]
        private static extern IntPtr GetShellWindow();
    }
}