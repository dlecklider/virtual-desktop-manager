# Virtual Desktop Manager

### Purpose

I often have multiple tasks or things on my todo-list concurrently. It is often very hard to juggle the various windows and applications I have open with a single desktop (even with multiple monitors). I found that a workflow involving several Virtual Desktop's helps reduce the cognitive load needed at any given moment during the day. The problem with this is when moving Windows between virtual desktops there is no easy way to do this. The user must open the Virtual Desktop task view, and physically drag the window to the appropriate virtual desktop. This can be tedius and annoying, especially in a keyboard centric workflow.

This application utilizes some hidden Windows 10 API's to hook into the movement and switching between virtual desktops.

### Usage

Currently the project relies off of a single `app.config` file to configure what keyboard shortcuts the user wants to utilize. 

NOTE: I am currently using this as a stop gap. There are still features I would like to add and support. This means that the app supports moving between Virtual Desktops by numeric value, and moving windows between desktops by numeric value. Currently directional (left/right) window moving is not yet supported.

### Pre-Requisites

- Windows 10 build 19041 (20H1) or later
- .NET == 6.0

### Build Instructions

The following commands will build a debug version of the application.

``` Powershell
git clone git@gitlab.com:dlecklider/virtualdesktopmanager.git
cd virtualdesktopmanager
dotnet restore
dotnet build
```

### Deployment

The following commands will build a release version of the application for distribution/publishing.

``` Powershell
git clone git@gitlab.com:dlecklider/virtualdesktopmanager.git
cd virtualdesktopmanager
dotnet restore
dotnet publish -c Release
```
